<?php

/*
 * Make our YUI grid style variables available
 */
function yui_style_html_5_process_page(&$variables) {
  $variables['yui_page_width'] = theme_get_setting ('yui_page_width');
  $variables['yui_sidebar'] = theme_get_setting ('yui_sidebar');
}


/*
 * Make it easier to use blocks in grids
 */
function yui_style_html_5_preprocess_block (&$variables) {
  $variables['classes_array'][] = 'yui-u';
  if ($variables['elements']['#weight'] == 1)
    $variables['classes_array'][] = 'first';
}

/*
 * Place your other theme hooks here.
 */
