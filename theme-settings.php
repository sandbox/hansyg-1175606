<?php
// YUI Style theme settings file.

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function yui_style_html_5_form_system_theme_settings_alter(&$form, $form_state) {
  $form['yui_style_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Location and width',
    // Make this the first option.
    '#weight' => -2,
  );
  $form['yui_style_settings']['yui_page_width'] = array(
    '#type' => 'select',
    '#title' => t('Page width'),
    '#default_value' => theme_get_setting('yui_page_width'),
    '#options' => array ('doc' => '750px width, centered', 'doc2' => '950px width, centered', 'doc3' => '100% width - Responsive', 'doc4' => '974px width, centered'),
  );
  $form['yui_style_settings']['yui_sidebar'] = array(
    '#type' => 'select',
    '#title' => t('Sidebar'),
    '#description' => t('Location and width'),
    '#default_value' => theme_get_setting('yui_sidebar'),
    '#options' => array ('' => 'no sidebar', 'yui-t1' => '160px, on left', 'yui-t2' => '180px, on left', 'yui-t3' => '300px, on left', 'yui-t4' => '180px, on right', 'yui-t5' => '240px, on right', 'yui-t6' => '300px, on right'),
  );
}
